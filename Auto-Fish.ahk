#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
#SingleInstance force

; NOTE: In autohotkey, the following special characters (usually) represent modifier keys:
; # is the WIN key. (it can mean other things though, as you can see above.)
; ^ is CTRL
; ! is ALT
; + is SHIFT
; list of other keys: http://www.autohotkey.com/docs/Hotkeys.htm

DetectHiddenWindows, On
; #MaxThreadsPerHotkey 2

ScreenTimerTime := 0
PauseCheck := 0
Total := GetConfig("Total")
Session := 0
Shinies := GetConfig("Shinies")
Targets := GetConfig("Targets")
SEncounters := GetConfig("SEncounters")
TEncounters := GetConfig("TEncounters")
STargets := 0
SShinies := 0
TargetAvg := Floor(Session / STargets)
Target := GetConfig("Target")
Confirm := 0

if (Target = "") ; Target select upon running the script for the first time
{
	ButtonTargetSelect:
	Gui, Destroy
	Gui, New
	Gui, Add, Text,, Submit a target pokemon to hunt.`nCase sensitive. (e.g: Bagon)`nIf you are explicitly hunting for shinies, submit "Shiny".
	Gui, Add, Text,, Target:
	Gui, Add, Edit, vTarget,
	Gui, Add, Button, Default, Set Target
	Gui, Show
	Return

	ButtonSetTarget:
	Gui, Submit, Nohide
	Gui, Destroy
	Return
}

Pause:: ; Manual Reload
WriteConfig("Total", Total)
WriteConfig("Shinies", Shinies)
WriteConfig("Targets", Targets)
WriteConfig("SEncounters", SEncounters)
WriteConfig("TEncounters", TEncounters)
WriteConfig("Target", Target)
Reload
Return

!^t:: ; Target set button
Gui, Destroy
Gui, New
Gui, Add, Text,, Submit a target pokemon to hunt.`nCase sensitive. (e.g: Bagon)`nIf you are explicitly hunting for shinies, submit "Shiny".
Gui, Add, Text,, Target:
Gui, Add, Edit, vTarget,
Gui, Add, Button, Default, Set Target
Gui, Show
Return

!^r:: ; Hard reset all stats
Confirm := Confirm + 1
if Confirm < 2
{
	MsgBox Hard reset button pressed. If you would really like to reset your lifetime stats, press this keybind again. (Ctrl + Alt + R)
}
else if Confirm >= 2
{
	Total := 0
	Shinies := 0
	Targets := 0
	SEncounters := 0
	TEncounters := 0
	WriteConfig("Total", Total)
	WriteConfig("Shinies", Shinies)
	WriteConfig("Targets", Targets)
	WriteConfig("SEncounters", SEncounters)
	WriteConfig("TEncounters", TEncounters)
	MsgBox All stats reset.
}
Return

End:: ; Panic button (Pauses the script instead of killing it)
PauseCheck := 1
Return

Del::
MsgBox %TargetAvg%
Return

Ins:: ; Testing button
Move("a", 4)
Move("d", 2)
Return

#If isPokeOneActive()

Home:: ; Main script button
TargetImage := Target . ".png"
Total := GetConfig("Total")
Shinies := GetConfig("Shinies")
Targets := GetConfig("Targets")
SEncounters := GetConfig("SEncounters")
TEncounters := GetConfig("TEncounters")
ScreenTimer()
Loop
{
	Confirm := 0
	PauseCheck()
	if DetectHealth() = 1
	{
		Session := Session + 1
		Total := Total + 1
		Sleep, 500
		if DetectTransparentImage("Shiny.png") = 1
		{
			SoundPlay, sounds\hey-lemonade.wav
			SplashTextOn, 350, 50, Shiny, You encountered a shiny! It took %SEncounters% encounters since the last one (or since the script started.)
			Shinies := Shinies + 1
			SEncounters := 0
			SShinies := SShinies + 1
			WriteConfig("Shinies", Shinies)
			WriteConfig("SEncounters", SEncounters)
			Sleep, 100
			Targets := GetConfig("Shinies")	
			SEncounters := GetConfig("SEncounters")
			Sleep, 5000
			SplashTextOff
			StopScreenTimer()
			Exit
		}
		else if DetectTransparentImage(TargetImage) = 1
		{
			SoundPlay, sounds\ice-pen.wav
			SplashTextOn, 350, 50, Target, Target Pokemon found! It took %TEncounters% encounters since the last one (or since the script started.)
			Targets := Targets + 1
			TEncounters := 0
			STargets := STargets + 1
			WriteConfig("Targets", Targets)
			WriteConfig("TEncounters", TEncounters)
			Sleep, 100
			Targets := GetConfig("Targets")	
			TEncounters := GetConfig("TEncounters")
			Sleep, 3000
			SplashTextOff
			StopScreenTimer()
			Exit
		}
		TEncounters := TEncounters + 1
		SEncounters := SEncounters + 1
		PauseCheck()
		Run()
		Sleep, 2500
		PauseCheck()
	}
	PauseCheck()
	Move("a", 2)
	PauseCheck()
	Move("d", 2)
	PauseCheck()
	Send {s down}
  Sleep, 50
  Send {s up}
  Send 1
  Sleep, 500
}
Return

#If

; Return 1 if the image is detected
DetectImage(image) {
  global
  ImageSearch, FoundX, FoundY, 0, 0, 1920, 1080, *32 images/%image%
  if (ErrorLevel=0)
  {
    return 1
  }
  return 0
}

DetectTransparentImage(image) ; Return 1 if the image is detected
{
  global
  ImageSearch, FoundX, FoundY, 0, 0, 350, 150, *TransBlack images/pokemon/%image%
  if (ErrorLevel=0)
  {
    return 1
  }
  return 0
}

Move(Direction, Amount) ; Condensed function to move in any direction
{
  global
	Loop, %Amount%
	{
		Send {%Direction% down}
		Random, sleep, 150, 250
		Sleep, %sleep%
		Send {%Direction% up}
		Random, sleep, 40, 60
		Sleep, %sleep%
	}
}

Run() ; Flee the battle
{
	global
	Loop, 10
	{
		if DetectImage("run.png") = 1
		{
			Random, xcoord, 1080, 1182
			Random, ycoord, 1026, 1053
			Click, %xcoord%, %ycoord%
			Sleep, 1000
			break
		}
		else
		{
			Sleep, 100
		}
	}
}

PauseCheck() ; Checks for an input to pause the script within the loop
{
	global
	if PauseCheck > 0
	{
		PauseCheck := 0
		SplashTextOn, 200, 25, Splash, Script paused.
		Sleep, 500
		SplashTextOff
		WriteConfig("Total", Total)
		WriteConfig("Shinies", Shinies)
		WriteConfig("Targets", Targets)
		WriteConfig("SEncounters", SEncounters)
		WriteConfig("TEncounters", TEncounters)
		StopScreenTimer()
		Exit
	}
}


isPokeOneActive() ; Hotkeys are disabled while not playing PokeOne
{
	if WinActive("ahk_exe PokeOne.exe") = 0x0 ; 0x430d46
		return False
	return True
}

DetectHealth() ; Detects a specific pixel color for the HP bar
{
  global
  PixelSearch, x, y, 0, 0, 350, 150, 0x39B01B, 4, Fast
  if ErrorLevel = 0
  {
      Return 1
  }
  return 0
}

ScreenTimer(textFunction := False) ; Stat tracker display
{
  global
  if (ScreenTimerActive = True) ; don't init screen timer again
  Return
  ScreenTimerActive := True
  ScreenTimerCustomBGColor := "c2ecc71" 
  ScreenTimerCustomTextColor := "caa1d1d" 
  ScreenTimerCustomSize := "28" 
  ScreenTimerCustomFont := "Segoe UI Bold"
  ScreenTimerGuiWidth := 300
  ScreenTimerGuiHeight := 320
  StatTrackerCustomTextColor := "caa1d1d" 
  StatTrackerCustomSize := "14" 
  StatTrackerCustomFont := "Arial"
  StatTrackerGuiWidth := 300
  Gui, Timer:+AlwaysOnTop +ToolWindow -Caption
  Gui, Timer:Margin, 0, 0
  Gui, Timer:Color, %ScreenTimerCustomBGColor%
  Gui, Timer:Font, s%ScreenTimerCustomSize% Q4, %ScreenTimerCustomFont%
  Gui, Timer:Add, Text, w%ScreenTimerGuiWidth% h55 vTimeSpent Center %ScreenTimerCustomTextColor%, Uptime: 00:00:00
  Gui, Timer:Font, s%StatTrackerCustomSize% Q4, %StatTrackerCustomFont%
  Gui, Timer:Add, Text, w%StatTrackerGuiWidth% h170 vSession Center %StatTrackerCustomTextColor%, Target Pokemon: %Target%`nSession Encounters: %Session%`nSession Targets: %STargets%`nAvg. Encounters per Target: %TargetAvg%`nEncounters since last target: %TEncounters%`nSession Shinies: %SShinies%`nEncounters since last shiny: %SEncounters%
  Gui, Timer:Add, Text, w%StatTrackerGuiWidth% h100 vTargets Center %StatTrackerCustomTextColor%, Lifetime Encounters: %Total%`nLifetime Targets: %Targets%`nLifetime Shinies: %Shinies%
  x := A_ScreenWidth - ScreenTimerGuiWidth
  y := 0
  Gui, Timer:Show, NoActivate w%ScreenTimerGuiWidth% h%ScreenTimerGuiHeight% y%y% x%x%, Session Timer & Stat Tracker
  SetTimer, TimerFunction, 999
  SetTimer, StatFunction, 999
}

StopScreenTimer() ; Pauses and disables the stat tracker
{
  global ScreenTimerActive 
  SetTimer, TimerFunction, Off
  Gui, Timer:Destroy
  ScreenTimerActive := False
}

TimerFunction() ; Updates the time on the stat tracker
{
  global ScreenTimerTime
  ScreenTimerTime := ScreenTimerTime + 1
  GuiControl, Timer:, TimeSpent, % "Uptime: " FormatSeconds(ScreenTimerTime)
}

StatFunction() ; Updates the stats on the stat tracker
{
  global TEncounters
	global SEncounters
	global Session
	global Shinies
	global Targets
	global Target
	global STargets
	global SShinies
	global Total
	global TargetAvg
	if STargets = 0
	{
		TargetAvg := Floor(Session / (STargets + 1))
	}
	else
	{
		TargetAvg := Floor(Session / STargets)
	}
	GuiControl, Timer:, Session, Target Pokemon: %Target%`nSession Encounters: %Session%`nSession Targets: %STargets%`nAvg. Encounters per Target: %TargetAvg%`nEncounters since last target: %TEncounters%`nSession Shinies: %SShinies%`nEncounters since last shiny: %SEncounters%
  GuiControl, Timer:, Targets, Lifetime Encounters: %Total%`nLifetime Targets: %Targets%`nLifetime Shinies: %Shinies%
}

FormatSeconds(NumberOfSeconds) ; Convert the specified number of seconds to hh:mm:ss format.
{
  time := 19990101 ; *Midnight* of an arbitrary date.
  time += NumberOfSeconds, seconds
  FormatTime, mmss, %time%, mm:ss
  hours := NumberOfSeconds//3600
  if (hours > 10)
    return hours ":" mmss
  else
    return "0" hours ":" mmss
}

GetConfig(var) ; Pulls stats from config.ini
{
  IniRead, out, % "config.ini", StatTracker, %var%
  Return out
}

WriteConfig(var, stat) ; Stores stats from config.ini
{
  IniWrite %stat%, % "config.ini", StatTracker, %var%
}